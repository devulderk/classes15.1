﻿namespace Classes._15._1
{
    class Teller
    {

        private int _tel;

        public Teller()
        {
            this.Tel = 0;
        }

        public int Tel
        {
            get { return _tel; }
            set { _tel = value; }
        }

        public void Resetten()
        {
            this.Tel = 0;

        }

        public void Verhoog()
        {
            this.Tel++;
        }

        public void Verlaag()
        {
            this.Tel--;
        }
    }
}
