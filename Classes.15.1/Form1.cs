﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Classes._15._1
{
    public partial class Form1 : Form
    {
        Teller t1 = new Teller();

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnLezen_Click(object sender, EventArgs e)
        {
            MessageBox.Show(t1.Tel.ToString(), "Waarde Teller", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void btnVerhoog_Click(object sender, EventArgs e)
        {
            t1.Verhoog();
            MessageBox.Show(t1.Tel.ToString(), "Waarde Teller", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }

        private void btnVerlaag_Click(object sender, EventArgs e)
        {
            t1.Verlaag();
            MessageBox.Show(t1.Tel.ToString(), "Waarde Teller", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            t1.Resetten();
            MessageBox.Show(t1.Tel.ToString(), "Waarde Teller", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void btnEinde_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
    }
}
